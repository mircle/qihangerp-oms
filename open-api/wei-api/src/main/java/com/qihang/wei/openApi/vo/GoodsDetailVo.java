package com.qihang.wei.openApi.vo;

import lombok.Data;

@Data
public class GoodsDetailVo extends BaseResVo {

    private ProductVo product;

}
