package com.qihang.wei.openApi.bo;

import lombok.Data;

@Data
public class OrderDetailBo {
    private String order_id;
}
