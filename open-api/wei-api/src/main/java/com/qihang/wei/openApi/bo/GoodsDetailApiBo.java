package com.qihang.wei.openApi.bo;

import lombok.Data;

@Data
public class GoodsDetailApiBo {
    private Integer data_type;
    private String product_id;

}
