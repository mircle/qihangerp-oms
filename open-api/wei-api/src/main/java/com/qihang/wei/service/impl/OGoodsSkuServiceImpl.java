package com.qihang.wei.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qihang.wei.domain.OGoodsSku;
import com.qihang.wei.service.OGoodsSkuService;
import com.qihang.wei.mapper.OGoodsSkuMapper;
import org.springframework.stereotype.Service;

/**
* @author TW
* @description 针对表【o_goods_sku(商品规格库存管理)】的数据库操作Service实现
* @createDate 2024-03-29 11:41:24
*/
@Service
public class OGoodsSkuServiceImpl extends ServiceImpl<OGoodsSkuMapper, OGoodsSku>
    implements OGoodsSkuService{

}




