package com.qihang.wei.service;

import com.qihang.wei.domain.OGoodsSku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author TW
* @description 针对表【o_goods_sku(商品规格库存管理)】的数据库操作Service
* @createDate 2024-03-29 11:41:24
*/
public interface OGoodsSkuService extends IService<OGoodsSku> {

}
