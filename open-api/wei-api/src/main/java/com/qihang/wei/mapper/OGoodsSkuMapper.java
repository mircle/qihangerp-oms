package com.qihang.wei.mapper;

import com.qihang.wei.domain.OGoodsSku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author TW
* @description 针对表【o_goods_sku(商品规格库存管理)】的数据库操作Mapper
* @createDate 2024-03-29 11:41:24
* @Entity com.qihang.wei.domain.OGoodsSku
*/
public interface OGoodsSkuMapper extends BaseMapper<OGoodsSku> {

}




