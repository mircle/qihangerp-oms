package com.qihang.wei.openApi.vo;

import com.alibaba.fastjson2.JSONObject;
import lombok.Data;

@Data
public class OrderDetailVo extends BaseResVo {

    private OrderVo order;

}
